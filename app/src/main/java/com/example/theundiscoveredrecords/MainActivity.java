package com.example.theundiscoveredrecords;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.theundiscoveredrecords.api.HttpApi;
import com.google.gson.JsonObject;

public class MainActivity extends AppCompatActivity {
    Button btRegister;
    Button btLogin;

    EditText etLoginLog;
    EditText etPasswordLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btRegister = (Button) findViewById(R.id.btreg);
        btLogin = (Button) findViewById(R.id.btlogin);
        etLoginLog = (EditText) findViewById(R.id.etLoginLog);
        etPasswordLog = (EditText) findViewById(R.id.etPasswordLog);

        btRegister.setOnClickListener(buttonRegClickListener);
        btLogin.setOnClickListener(buttonLoginClickListener);

        //UserDao.AppDatabase db = DbHandler.getInstance().getDatabase();
        //UserDao user = db.userDao();
        //User userData = user.getById(0);

        //String username = userData.getUsername();
        //String password = userData.getPassword();

        //если юзер уже есть в БД - отправляем запрос на аутентификацию, если нет - идём в MainActivity
        //if(username != null && password != null){
        //    HttpApi requestApi = new HttpApi();
        //    JsonObject request = requestApi.HttpLogin(username, password);
        //    if(request.get("status").getAsString().equals("ok")){
        //        Intent intent = new Intent(MainActivity.this, AccountActivity.class);
        //        startActivity(intent);
        //    }
        //}
    }


    View.OnClickListener buttonRegClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View regClickV){
            Intent intent = new Intent(MainActivity.this, AccountActivity.class);
            startActivity(intent);
            finish();
        }
    };

    View.OnClickListener buttonLoginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View logClickV){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpApi requestApi = new HttpApi();
            JsonObject request = requestApi.HttpLogin(etLoginLog.getText().toString(), etPasswordLog.getText().toString());
            if(request.get("status").getAsString().equals("ok")){
                //если юзер залогинился - отправляем его в локальную БД
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //UserDao.AppDatabase db = DbHandler.getInstance().getDatabase();
                        //UserDao userDao = db.userDao();
                        //userDao.createUser();
                        //userDao.setUser(etLoginLog.getText().toString(), etPasswordLog.getText().toString());
                    }
                }).start();
                Intent intent = new Intent(MainActivity.this, AccountActivity.class);
                startActivity(intent);
            }
            if(request.get("status").getAsString().equals("fail")){
                {
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                    alertDialog.setMessage("Неправильный логин или пароль!\n" + request.get("status").getAsString());
                    alertDialog.setTitle("Ошибка входа в систему");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    alertDialog.create().show();
                }
            }
        }
    };
}
