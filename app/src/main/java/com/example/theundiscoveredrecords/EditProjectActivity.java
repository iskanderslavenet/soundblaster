package com.example.theundiscoveredrecords;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.theundiscoveredrecords.api.HttpApi;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;

public class EditProjectActivity extends AppCompatActivity {

    ImageView ivEditProjectAvatar;
    TextInputEditText projectName, projectBand, textAuthor, musicAutor, text, comment;
    Intent fileIntent;
    Uri filePath;

    ProjectItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_project);

        ivEditProjectAvatar = (ImageView) findViewById(R.id.ivEditProjectAvatar);
        projectName = (TextInputEditText) findViewById(R.id.tiEditProjectName);
        projectBand = (TextInputEditText) findViewById(R.id.tiEditProjectBand);
        textAuthor = (TextInputEditText) findViewById(R.id.tiEditProjectTextAuthor);
        musicAutor = (TextInputEditText) findViewById(R.id.tiEditProjectMusicAuthor);
        text = (TextInputEditText) findViewById(R.id.tiEditSongText);
        comment = (TextInputEditText) findViewById(R.id.tiEditProjectComment);

        Bundle intent = getIntent().getExtras();
        item = intent.getParcelable("Song");
        //byte[] avatar = intent.getByteArray("Avatar");
        //ivEditProjectAvatar.setImageBitmap(getBitmapFromByteArray(avatar));
        projectName.setText(item.getProjectName());
        projectBand.setText(item.getProjectBrand());
        textAuthor.setText(item.getTextAuthor());
        musicAutor.setText(item.getMusicAuthor());
        text.setText(item.getSongText().toString());
        comment.setText(item.getProjectComment());

        getAvatar(item.getProjectName(),"");

        ivEditProjectAvatar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //выбираем аватар
                fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
                fileIntent.setType("*/*");
                startActivityForResult(fileIntent, 8);
            }
        });
    }

    public void getAvatar(String projectName, String projectMusician){
        HttpApi httpApi = new HttpApi();
        Bitmap avatar;
        avatar = httpApi.getProjectAvatar(projectName, "iskanderslavenet");
        if(avatar == null){
            return;
        }
        try{
            ivEditProjectAvatar.setImageBitmap(avatar);
        }
        catch(Exception ex){

        }
    }

    public Bitmap getBitmapFromByteArray(byte[] avatar){
        ByteArrayInputStream stream = new ByteArrayInputStream(avatar, 0, avatar.length);
        Bitmap bitmap = BitmapFactory.decodeStream(stream);
        return bitmap;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode){
            case 8:
                Log.i("REQUEST_CODE", "7");
                if(resultCode == RESULT_OK){
                    filePath = data.getData();
                    try{
                        Bitmap avatar = BitmapFactory.decodeStream(getContentResolver().openInputStream(filePath));
                        ivEditProjectAvatar.setImageBitmap(avatar);
                    }
                    catch(FileNotFoundException ex) { }
                }
                break;
        }
    }

    public void saveProject(View v){
        JsonObject project = new JsonObject();
        JSONObject jsonObject = new JSONObject();
        JsonElement jsonElement;
        try{
            jsonObject.put("project_id", item.getProjectId());
            jsonObject.put("project_name", projectName.getText().toString());
            jsonObject.put("project_band", projectBand.getText().toString());
            jsonObject.put("project_text_author", textAuthor.getText().toString());
            jsonObject.put("project_music_author", musicAutor.getText().toString());
            jsonObject.put("project_text", text.getText().toString());
            //jsonObject.put("project_comment", text.getText().toString());
        }
        catch(JSONException ex){ }

        HttpApi api = new HttpApi();
        try{
            JsonObject resp = api.UpdateProject("iskanderslavenet", jsonObject);
            String id = resp.get("projectIds").getAsString();
            Log.i("ID", id);
            SaveAvatar(id);
        }
        catch (NullPointerException ex) {
            Log.i("FFF",ex.getMessage());
        }
    }

    public void SaveAvatar(String id){
        File avatar = new File(filePath.getPath());
        Bitmap ava = ((BitmapDrawable)ivEditProjectAvatar.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ava.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] image = stream.toByteArray();
        Log.i("FILE", avatar.getAbsolutePath());
        HttpApi api = new HttpApi();
        Log.i("path", avatar.toString());
        api.UploadProjectAvatar(id, image);
    }
}
