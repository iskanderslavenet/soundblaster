package com.example.theundiscoveredrecords;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.theundiscoveredrecords.adapters.ProjectRvAdapter;
import com.example.theundiscoveredrecords.api.HttpApi;
import com.google.gson.JsonObject;

import java.util.List;

public class AccountActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;

    ImageView imageView, ivUserType, ivEmail, ivPhone, ivCity, ivCountry;
    TextView userName, userSurname, nickName, userType, Email, Phone, City, Country;
    RecyclerView rvProjects;
    RecyclerView.Adapter rvAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account2);

        imageView = (ImageView) findViewById(R.id.imgAvatar);
        ivUserType = (ImageView) findViewById(R.id.ivUserType1);
        ivEmail = (ImageView) findViewById(R.id.ivEmail);
        ivPhone = (ImageView) findViewById(R.id.ivPhone);
        ivCity = (ImageView) findViewById(R.id.ivCity);
        ivCountry = (ImageView) findViewById(R.id.ivCounty);
        userName = (TextView) findViewById(R.id.tvName);
        userSurname = (TextView) findViewById(R.id.tvSurname);
        userType = (TextView) findViewById(R.id.tvUserType);
        Email = (TextView) findViewById(R.id.tvEmail);
        Phone = (TextView) findViewById(R.id.tvPhone);
        City = (TextView) findViewById(R.id.tvCity);
        Country = (TextView) findViewById(R.id.tvCountry);
        nickName = (TextView) findViewById(R.id.tvNickName);
        rvProjects = (RecyclerView) findViewById(R.id.rvProjects);

        rvProjects.setLayoutManager(new LinearLayoutManager(this));
        rvProjects.setHasFixedSize(true);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);


        getAccountInfo();
        getAvatar();
        getProjects();
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getAccountInfo();
                getAvatar();
                getProjects();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }

    public void getAccountInfo(){
        HttpApi httpApi = new HttpApi();
        JsonObject account = httpApi.GetAccount("iskanderslavenet", "testpasswd");
        userName.setText(account.get("firstName").getAsString());
        userSurname.setText(account.get("lastName").getAsString());
        Email.setText("Почта: " + account.get("email").getAsString());
        userType.setText("Профессия: " + account.get("userType").getAsString());
        Phone.setText("Телефон: " + account.get("phone").getAsString());
        Country.setText("Страна: " + account.get("country").getAsString());
        City.setText("Город: " + account.get("city").getAsString());
        nickName.setText(account.get("username").getAsString());
    }

    public void getAvatar(){
        HttpApi httpApi = new HttpApi();
        Bitmap avatar;
        avatar = httpApi.getAvatar("iskanderslavenet", "testpasswd");
        if(avatar == null){
            imageView.setImageResource(android.R.drawable.zoom_plate);
            return;
        }
        try{
            imageView.setImageBitmap(avatar);
        }
        catch(Exception ex){
            imageView.setImageResource(android.R.drawable.zoom_plate);
        }
    }

    public void rvClick(int position, List<ProjectItem> projects){
        Intent intent = new Intent(AccountActivity.this, ProjectActivity.class);
        intent.putExtra("Song", projects.get(position));
        startActivity(intent);
    }

    public void getProjects(){
        HttpApi httpApi = new HttpApi();
        final List<ProjectItem> projects;
        projects = httpApi.GetProjects("iskanderslavenet", "testpasswd");
        rvAdapter = new ProjectRvAdapter(projects, AccountActivity.this);
        ((ProjectRvAdapter) rvAdapter).setOnItemClickListener(new ProjectRvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                rvClick(position, projects);
            }
        });
        rvProjects.setAdapter(rvAdapter);
    }

    public void goToEdit(View view){
        Intent intent = new Intent(AccountActivity.this, AccountEditActivity.class);
        intent.putExtra("firstName", userName.getText().toString());
        intent.putExtra("lastName", userSurname.getText().toString());
        intent.putExtra("userType", userType.getText().toString());
        intent.putExtra("email", Email.getText().toString());
        intent.putExtra("phone", Phone.getText().toString());
        intent.putExtra("city", City.getText().toString());
        intent.putExtra("country", Country.getText().toString());
        startActivity(intent);
    }

    public void AddProject(View view){
        Intent intent = new Intent(AccountActivity.this, AddProjectActivity.class);
        startActivity(intent);
    }
}
