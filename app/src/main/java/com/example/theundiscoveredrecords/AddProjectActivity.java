package com.example.theundiscoveredrecords;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.theundiscoveredrecords.api.HttpApi;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okio.Buffer;
import okio.BufferedSink;

public class AddProjectActivity extends AppCompatActivity {

    ImageView ivAddProjectAvatar;
    TextInputEditText projectName, projectBand, textAuthor, musicAutor, text, comment;
    Intent fileIntent;
    Uri filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_project);

        ivAddProjectAvatar = (ImageView) findViewById(R.id.ivAddProjectAvatar);
        projectName = (TextInputEditText) findViewById(R.id.tiProjectName);
        projectBand = (TextInputEditText) findViewById(R.id.tiProjectBand);
        textAuthor = (TextInputEditText) findViewById(R.id.tiProjectTextAuthor);
        musicAutor = (TextInputEditText) findViewById(R.id.tiProjectMusicAuthor);
        text = (TextInputEditText) findViewById(R.id.tiSongText);
        comment = (TextInputEditText) findViewById(R.id.tiProjectComment);

        ivAddProjectAvatar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //выбираем аватар
                fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
                fileIntent.setType("*/*");
                startActivityForResult(fileIntent, 7);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode){
            case 7:
                Log.i("REQUEST_CODE", "7");
                if(resultCode == RESULT_OK){
                    filePath = data.getData();
                    try{
                        Bitmap avatar = BitmapFactory.decodeStream(getContentResolver().openInputStream(filePath));
                        ivAddProjectAvatar.setImageBitmap(avatar);
                    }
                    catch(FileNotFoundException ex) { }
                }
                break;
        }
    }

    public void SaveProject(View v){
        JsonObject project = new JsonObject();
        JSONObject jsonObject = new JSONObject();
        JsonElement jsonElement;
        try{
            jsonObject.put("project_name", projectName.getText().toString());
            jsonObject.put("project_band", projectBand.getText().toString());
            jsonObject.put("project_text_author", textAuthor.getText().toString());
            jsonObject.put("project_music_author", musicAutor.getText().toString());
            jsonObject.put("project_text", text.getText().toString());
            jsonObject.put("project_comment", text.getText().toString());
        }
        catch(JSONException ex){ }

        HttpApi api = new HttpApi();
        try{
            JsonObject resp = api.AddProject("iskanderslavenet", jsonObject);
            String id = resp.get("projectIds").getAsString();
            Log.i("ID", id);
            SaveAvatar(id);
        }
        catch (NullPointerException ex) {
            Log.i("FFF",ex.getMessage());
        }
    }

    public void SaveAvatar(String id){
        File avatar = new File(filePath.getPath());
        Bitmap ava = ((BitmapDrawable)ivAddProjectAvatar.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ava.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] image = stream.toByteArray();
        Log.i("FILE", avatar.getAbsolutePath());
        HttpApi api = new HttpApi();
        Log.i("path", avatar.toString());
        api.UploadProjectAvatar(id, image);
    }
}
