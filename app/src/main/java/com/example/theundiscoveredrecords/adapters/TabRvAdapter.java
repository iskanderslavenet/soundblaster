package com.example.theundiscoveredrecords.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.theundiscoveredrecords.R;

import java.util.List;

public class TabRvAdapter extends RecyclerView.Adapter<TabRvAdapter.ViewHolderTabs> {
    List<String> tabList;
    private Context context;

    public TabRvAdapter(List<String> tabList, Context context) {
        this.tabList = tabList;
        this.context = context;
    }

    private OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    @NonNull
    @Override
    public ViewHolderTabs onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tabs_adapter, parent, false);
        return new ViewHolderTabs(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderTabs holder, int position) {
        String tabName = tabList.get(position);
        holder.tabName.setText(tabName);
        Log.i("ADAPTER!", tabName);
    }

    @Override
    public int getItemCount() {
        return tabList.size();
    }

    public class ViewHolderTabs extends RecyclerView.ViewHolder{
        TextView tabName;

        public ViewHolderTabs(@NonNull View itemView, final TabRvAdapter.OnItemClickListener listener) {
            super(itemView);
            tabName = (TextView) itemView.findViewById(R.id.tvTabFileName);

            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
