package com.example.theundiscoveredrecords.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.theundiscoveredrecords.ProjectItem;
import com.example.theundiscoveredrecords.R;

import java.util.List;

public class ProjectRvAdapter extends RecyclerView.Adapter<ProjectRvAdapter.ViewHolder> {
    private List<ProjectItem> projectItemList;
    private Context context;

    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public ProjectRvAdapter(List<ProjectItem> projectItemList, Context context) {
        this.projectItemList = projectItemList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.project_item, parent, false);
        return new ViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProjectItem projectItem = projectItemList.get(position);
        holder.projectName.setText(projectItem.getProjectName());
        holder.projectMusician.setText(projectItem.getProjectMusician());
        holder.projectComment.setText(projectItem.getProjectComment());
       // holder.projectAvatar.setImageResource(R.drawable.project_avatar);
    }

    @Override
    public int getItemCount() {
        return projectItemList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView projectName;
        TextView projectMusician;
        TextView projectComment;
        ImageView projectAvatar;

        public ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            projectName = (TextView) itemView.findViewById(R.id.tvProjectName);
            projectMusician = (TextView) itemView.findViewById(R.id.tvProjectMusician);
            projectComment = (TextView) itemView.findViewById(R.id.tvProjectComment);
            //projectAvatar = (ImageView) itemView.findViewById(R.id.ivProjectAvatar);

            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });

        }
    }
}
