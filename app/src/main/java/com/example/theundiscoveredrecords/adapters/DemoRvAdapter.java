package com.example.theundiscoveredrecords.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.theundiscoveredrecords.R;

import java.util.List;

public class DemoRvAdapter extends RecyclerView.Adapter<DemoRvAdapter.ViewHolderDemos> {
    List<String> demoList;
    private Context context;

    public DemoRvAdapter(List<String> demoList, Context context) {
        this.demoList = demoList;
        this.context = context;
    }

    private OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    @NonNull
    @Override
    public ViewHolderDemos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.demos_adapter, parent, false);
        return new ViewHolderDemos(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull DemoRvAdapter.ViewHolderDemos holder, int position) {
        String tabName = demoList.get(position);
        holder.demoName.setText(tabName);
        Log.i("ADAPTER!", tabName);
    }

    @Override
    public int getItemCount() {
        return demoList.size();
    }

    public class ViewHolderDemos extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView demoName;
        ImageButton btnPlayDemo;
        boolean playerStatus = false; //false - pause, true - play
        boolean initianStage = true;

        public ViewHolderDemos(@NonNull View itemView, final DemoRvAdapter.OnItemClickListener listener) {
            super(itemView);
            demoName = (TextView) itemView.findViewById(R.id.tvSongFileName);
            btnPlayDemo = (ImageButton) itemView.findViewById(R.id.btnPlayDemo);
            btnPlayDemo.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            playerStatus = !playerStatus;
            if (playerStatus) {
                btnPlayDemo.setImageResource(android.R.drawable.ic_media_play);
            } else {
                btnPlayDemo.setImageResource(android.R.drawable.ic_media_pause);
            }
        }
    }
}