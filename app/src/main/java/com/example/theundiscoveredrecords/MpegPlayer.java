package com.example.theundiscoveredrecords;

import android.media.MediaPlayer;

public class MpegPlayer {

    public void stopPlay(MediaPlayer mp){
        mp.stop();
        try{
            mp.prepare();
            mp.seekTo(0);
        }
        catch (Throwable throwable){

        }
    }

    public void play(MediaPlayer mp){
        mp.start();
    }

    public void pause(MediaPlayer mp){
        mp.pause();
    }

    public void stop(MediaPlayer mp){
        stopPlay(mp);
    }
}
