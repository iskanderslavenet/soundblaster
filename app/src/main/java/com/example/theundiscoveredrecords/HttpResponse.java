package com.example.theundiscoveredrecords;

public class HttpResponse {
    private String response;

    public void setResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }
}
