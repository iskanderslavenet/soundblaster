package com.example.theundiscoveredrecords.api;

import android.util.Log;

import com.example.theundiscoveredrecords.MyCookieJar;

import org.jetbrains.annotations.NotNull;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class HttpHandler {

    public ResponseBody BaseRequest(String urlString, RequestBody requestBody){
        Response resp = null;
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .cookieJar(new MyCookieJar(){
                    private final HashMap<HttpUrl, List<Cookie>> cookieStore = new HashMap<>();

                    @NotNull
                    @Override

                    public List<Cookie> loadForRequest(@NotNull HttpUrl httpUrl) {
                        List<Cookie> cookies = cookieStore.get(httpUrl);
                        return cookies != null ? cookies : new ArrayList<Cookie>();
                    }

                    @Override
                    public void saveFromResponse(@NotNull HttpUrl httpUrl, @NotNull List<Cookie> list) {
                        cookieStore.put(httpUrl, list);
                    }
                });

        OkHttpClient client = builder.build();

        Request req = new Request.Builder()
                .url(urlString)
                .post(requestBody)
                .build();

        CallbackFuture future = new CallbackFuture();
        client.newCall(req).enqueue(future);

        try{
            resp = future.get();
            Log.i("CONTENT-TYPE:", resp.header("content-type"));
        }
        catch(ExecutionException | InterruptedException e){ }

        return resp.body();
    }
}
