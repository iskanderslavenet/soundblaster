package com.example.theundiscoveredrecords.api;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.theundiscoveredrecords.ProjectItem;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSink;

public class HttpApi {
    //все запросы тут

    public JsonObject HttpLogin(String username, String password){

        JsonParser parser = new JsonParser();
        HttpHandler handler = new HttpHandler();
        ResponseBody response;
        String responseString = null;
        response = handler.BaseRequest("http://10.0.2.2:3000/login?username=" + username + "&password=" + password, new RequestBody() {
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {

            }
        });

        try{
            responseString = response.string();
        }
        catch(IOException ex){}

        if(response == null){
            JsonObject jObj = new JsonObject();
            jObj.addProperty("status", "null response");
            return jObj;
        }
        return parser.parse(responseString).getAsJsonObject();
    }

    public JsonObject HttpSignup(String username, String password, String email, String firstName, String lastName){
        HttpHandler handler = new HttpHandler();
        ResponseBody response;
        String responseString = null;
        RequestBody requestBody = new RequestBody() {
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {

            }
        };
        response = handler.BaseRequest("http://10.0.2.2:3000/login?username=" + username + "&password=" + password, requestBody);

        try{
            responseString = response.string();
        }
        catch(IOException ex){}

        if(response == null){
            JsonObject jObj = new JsonObject();
            jObj.addProperty("status", "null response");
            return jObj;
        }
        JsonParser parser = new JsonParser();
        return parser.parse(responseString).getAsJsonObject();
    }

    public JsonObject SignOut(){
        HttpHandler handler = new HttpHandler();
        ResponseBody response;
        String responseString = null;
        RequestBody requestBody = new RequestBody() {
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {

            }
        };
        response = handler.BaseRequest("http://10.0.2.2:3000/signout", requestBody);

        try{
            responseString = response.string();
        }
        catch(IOException ex){}

        if(response == null){
            JsonObject jObj = new JsonObject();
            jObj.addProperty("status", "null response");
            return jObj;
        }
        JsonParser parser = new JsonParser();
        return parser.parse(responseString).getAsJsonObject();
    }

    public Bitmap getAvatar(String username, String password){
        Bitmap avatar;
        JsonParser parser = new JsonParser();
        HttpHandler handler = new HttpHandler();
        JsonObject jsonObject = new JsonObject();
        ResponseBody response;
        response = handler.BaseRequest("http://10.0.2.2:3000/getaccount/getavatar?username=" + username + "&password=" + password, new RequestBody() {
            @Override
            public MediaType contentType() {
                return MediaType.parse("image/jpeg");
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {

            }
        });
        if(response == null){
            Log.i("[GETAVATAR]:", "null response");
        }
        avatar = BitmapFactory.decodeStream(response.byteStream());
        return avatar;
    }

    public Bitmap getProjectAvatar(String projectName, String musicianName){
        Bitmap avatar;
        JsonParser parser = new JsonParser();
        HttpHandler handler = new HttpHandler();
        JsonObject jsonObject = new JsonObject();
        ResponseBody response;
        response = handler.BaseRequest("http://10.0.2.2:3000/getproject/getavatar?projectname=" + projectName + "&projectmusician=" + musicianName, new RequestBody() {
            @Override
            public MediaType contentType() {
                return MediaType.parse("image/jpeg");
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {

            }
        });
        if(response == null){
            Log.i("[GETAVATAR]:", "null response");
        }
        avatar = BitmapFactory.decodeStream(response.byteStream());
        return avatar;
    }

    public List<ProjectItem> GetProjects(String username, String password){
        String responseBody = null;
        List<ProjectItem> projects = new ArrayList<ProjectItem>();
        JsonParser parser = new JsonParser();
        HttpHandler handler = new HttpHandler();
        JSONObject jsonObject;
        JSONArray jsonArray;
        ResponseBody response;
        response = handler.BaseRequest("http://10.0.2.2:3000/getprojects/?username=" + username + "&password=" + password, new RequestBody() {
            @Override
            public MediaType contentType() {
                return MediaType.parse("image/jpeg");
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {

            }
        });
        if(response == null){

        }
        try {
            responseBody = response.string();
            jsonArray = new JSONArray(responseBody);
            for(int i = 0; i < jsonArray.length(); i++){
                Log.i("CYCLE_IT", Integer.toString(i));
                String projectName = jsonArray.getJSONObject(i).getString("project_name");
                String projectMusician = jsonArray.getJSONObject(i).getString("project_brand");
                String projectComment = jsonArray.getJSONObject(i).getString("project_comment");
                String projectBrand = jsonArray.getJSONObject(i).getString("project_brand");
                Log.i("HMM", projectName);
                String projectText = jsonArray.getJSONObject(i).getString("song_text");
                String projectPath = jsonArray.getJSONObject(i).getString("project_path");
                String textAuthor = jsonArray.getJSONObject(i).getString("text_author");
                String musicAuthor = jsonArray.getJSONObject(i).getString("music_author");
                int projectId = jsonArray.getJSONObject(i).getInt("project_id");
                projects.add(new ProjectItem(projectName, projectMusician, projectComment, projectBrand, projectPath, projectText, textAuthor, musicAuthor, projectId));
            }
        }

        catch(IOException | JSONException ex){
            Log.i("FUCKING ERROR", ex.getMessage());
        }
        return projects;
    }

    public List<String> GetTabs(String projectName, String projecrMusician){
        String responseBody = null;
        List<String> tabs = new ArrayList<String>();
        HttpHandler handler = new HttpHandler();
        JSONArray jsonArray;
        ResponseBody response;
        response = handler.BaseRequest("http://10.0.2.2:3000/gettablist/?projectname=" + projectName + "&projectmusician=" + projecrMusician, new RequestBody() {
            @Override
            public MediaType contentType() {
                return MediaType.parse("image/jpeg");
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {

            }
        });
        if(response == null){

        }
        try {
            responseBody = response.string();
            jsonArray = new JSONArray(responseBody);
            for(int i = 0; i < jsonArray.length(); i++){
                Log.i("ARRAY", jsonArray.getString(i));
                tabs.add(jsonArray.getString(i));
            }
        }

        catch(IOException | JSONException ex){
            Log.i("FUCKING ERROR", ex.getMessage());
        }
        return tabs;
    }

    public List<String> GetDemos(String projectName, String projecrMusician){
        String responseBody = null;
        List<String> tabs = new ArrayList<String>();
        HttpHandler handler = new HttpHandler();
        JSONArray jsonArray;
        ResponseBody response;
        response = handler.BaseRequest("http://10.0.2.2:3000/getdemolist?projectname=" + projectName + "&projectmusician=" + projecrMusician, new RequestBody() {
            @Override
            public MediaType contentType() {
                return MediaType.parse("image/jpeg");
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {

            }
        });
        if(response == null){

        }
        try {
            responseBody = response.string();
            jsonArray = new JSONArray(responseBody);
            for(int i = 0; i < jsonArray.length(); i++){
                Log.i("ARRAY", jsonArray.getString(i));
                tabs.add(jsonArray.getString(i));
            }
        }

        catch(IOException | JSONException ex){
            Log.i("FUCKING ERROR", ex.getMessage());
        }
        return tabs;
    }

    public JsonObject GetAccount(String username, String password){
        JsonParser parser = new JsonParser();
        HttpHandler handler = new HttpHandler();
        ResponseBody response;
        String responseString = null;
        response = handler.BaseRequest("http://10.0.2.2:3000/getaccount?username=" + username + "&password=" + password, new RequestBody() {
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {

            }
        });

        try{
            responseString = response.string();
            Log.i("RESPONSE_STRING", responseString);
        }
        catch(IOException ex){}

        if(response == null){
            JsonObject jObj = new JsonObject();
            jObj.addProperty("status", "null response");
            return jObj;
        }
        return parser.parse(responseString).getAsJsonObject();
    }

    public JsonElement ChangeAccount(String username, String password, JSONObject jsonObject){
        RequestBody body = RequestBody.Companion.create(jsonObject.toString(), MediaType.parse("application/json"));
        JsonParser parser = new JsonParser();
        HttpHandler handler = new HttpHandler();
        ResponseBody response;
        String responseString = null;
        response = handler.BaseRequest("http://10.0.2.2:3000/changeaccount?username=" + username + "&password=" + password, body);

        try{
            responseString = response.string();
            Log.i("RESPONSE_STRING", responseString);
        }
        catch(IOException ex){}

        if(response == null){
            JsonObject jObj = new JsonObject();
            jObj.addProperty("status", "null response");
            return jObj;
        }
        return parser.parse(responseString).getAsJsonObject();
    }

    public JsonElement GetProject(String username, String password){
        JsonParser parser = new JsonParser();
        HttpHandler handler = new HttpHandler();
        ResponseBody response;
        String responseString = null;
        response = handler.BaseRequest("http://10.0.2.2:3000/getaccount?username=" + username + "&password=" + password, new RequestBody() {
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {

            }
        });

        try{
            responseString = response.string();
            Log.i("RESPONSE_STRING", responseString);
        }
        catch(IOException ex){}

        if(response == null){
            JsonObject jObj = new JsonObject();
            jObj.addProperty("status", "null response");
            return jObj;
        }
        return parser.parse(responseString).getAsJsonObject();
    }

    public JsonElement Upload(){
        return new JsonElement() {
            @Override
            public JsonElement deepCopy() {
                return null;
            }
        };
    }

    public JsonObject AddProject(String username, JSONObject jsonObject) throws NullPointerException{
        RequestBody body = RequestBody.Companion.create(jsonObject.toString(), MediaType.parse("application/json"));
        JsonParser parser = new JsonParser();
        HttpHandler handler = new HttpHandler();
        ResponseBody response;
        String responseString = null;
        response = handler.BaseRequest("http://10.0.2.2:3000/addproject?username=" + username, body);

        try{
            responseString = response.string();
            Log.i("RESPONSE_STRING", responseString);
        }
        catch(IOException ex){
            Log.i("RESPONSE_STRING", ex.getMessage());
        }

        if(response == null){
            JsonObject jObj = new JsonObject();
            jObj.addProperty("status", "null response");
            return jObj;
        }
        JsonObject resp = parser.parse(responseString).getAsJsonObject();
        return resp;
    }

    public JsonObject UpdateProject(String username, JSONObject jsonObject) throws NullPointerException{
        RequestBody body = RequestBody.Companion.create(jsonObject.toString(), MediaType.parse("application/json"));
        JsonParser parser = new JsonParser();
        HttpHandler handler = new HttpHandler();
        ResponseBody response;
        String responseString = null;
        response = handler.BaseRequest("http://10.0.2.2:3000/updateproject?username=" + username, body);

        try{
            responseString = response.string();
            Log.i("RESPONSE_STRING", responseString);
        }
        catch(IOException ex){
            Log.i("RESPONSE_STRING", ex.getMessage());
        }

        if(response == null){
            JsonObject jObj = new JsonObject();
            jObj.addProperty("status", "null response");
            return jObj;
        }
        JsonObject resp = parser.parse(responseString).getAsJsonObject();
        return resp;
    }

    private String bodyToString(RequestBody request){
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            copy.writeTo(buffer);
            return buffer.readUtf8();
        }
        catch (final IOException e) {
            return e.getMessage();
        }
    }

    public JsonObject UploadProjectAvatar(String projectIds, byte[] file) {
        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("avatar", "avatar.jpg", RequestBody.create(file, MediaType.parse("image/jpeg")))
                .build();
        Log.i("BODY STRING", bodyToString(body));
        JsonParser parser = new JsonParser();
        HttpHandler handler = new HttpHandler();
        ResponseBody response;
        String responseString = null;
        response = handler.BaseRequest("http://10.0.2.2:3000/uploadavatar?project_id=" + projectIds, body);
        Log.i("RESPONSE BODY", bodyToString(body));
        try{
            responseString = response.string();
            Log.i("RESPONSE_STRING", responseString);
        }
        catch(IOException ex){}

        if(response == null){
            JsonObject jObj = new JsonObject();
            jObj.addProperty("status", "null response");
            return jObj;
        }
        return parser.parse(responseString).getAsJsonObject();
    }

    public JsonObject UploadNewDemo(String projectIds, byte[] file, String fileName) {
        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("demo", fileName, RequestBody.create(file, MediaType.parse("image/jpeg")))
                .build();
        Log.i("BODY STRING", bodyToString(body));
        JsonParser parser = new JsonParser();
        HttpHandler handler = new HttpHandler();
        ResponseBody response;
        String responseString = null;
        response = handler.BaseRequest("http://10.0.2.2:3000/uploaddemo?project_id=" + projectIds, body);
        Log.i("RESPONSE BODY", bodyToString(body));
        try{
            responseString = response.string();
            Log.i("RESPONSE_STRING", responseString);
        }
        catch(IOException ex){}

        if(response == null){
            JsonObject jObj = new JsonObject();
            jObj.addProperty("status", "null response");
            return jObj;
        }
        return parser.parse(responseString).getAsJsonObject();
    }
}