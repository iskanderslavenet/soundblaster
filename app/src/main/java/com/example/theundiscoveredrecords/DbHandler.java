package com.example.theundiscoveredrecords;

import android.app.Application;

import androidx.room.Room;

public class DbHandler extends Application {
    public static DbHandler dbHandler;
    //private UserDao.AppDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        dbHandler = this;
        //database = Room.databaseBuilder(this, UserDao.AppDatabase.class, "dbuser")
        //        .build();
    }

    public static DbHandler getInstance() {
        return dbHandler;
    }

    //public UserDao.AppDatabase getDatabase() {
    //    return database;
    //}
}
