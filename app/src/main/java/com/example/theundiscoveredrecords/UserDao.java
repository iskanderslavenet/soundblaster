package com.example.theundiscoveredrecords;

import androidx.room.Dao;
import androidx.room.Database;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.RoomDatabase;

import java.util.List;

@Dao
public interface UserDao {
    @Insert
    void insertAll(User...users);

    @Delete
    void delete(User...user);

    @Query("SELECT * FROM User")
    List<User> getAllUsers();

    @Query("SELECT * FROM User WHERE id = :id")
    User getById(int id);

    //@Database(entities = {User.class}, version = 1)
    //public abstract class AppDatabase extends RoomDatabase{
    //    public abstract UserDao userDao();
    //}

    //@Query("CREATE TABLE User (username TEXT, password TEXT)")
    //User createUser();

    //@Query("INSERT INTO User (username=:username, password=:password)")
    //User setUser(String username, String password);
}
