package com.example.theundiscoveredrecords;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

public class MyCookieJar implements CookieJar {

    private List<Cookie> cookies;

    //сохранение cookie в лист из ответа
    @Override
    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
        this.cookies =  cookies;
    }

    //cookie для запроса
    @Override
    public List<Cookie> loadForRequest(HttpUrl url) {
        //если лист с cookie не пустой - возвращаем его
        //иначе возвращаем новый пустой лист
        if (cookies != null)
            return cookies;
        return new ArrayList<Cookie>();
    }
}
