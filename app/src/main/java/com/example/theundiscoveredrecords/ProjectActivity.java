package com.example.theundiscoveredrecords;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.OpenableColumns;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.theundiscoveredrecords.adapters.DemoRvAdapter;
import com.example.theundiscoveredrecords.adapters.TabRvAdapter;
import com.example.theundiscoveredrecords.api.HttpApi;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class ProjectActivity extends AppCompatActivity {

    TextView tvBand, tvSong, tvTextAuthor, tvMusicAuthor, tvSongText;
    ImageView projectAvatar;
    RecyclerView rvTabs, rvDemos;
    boolean playerStatus = false;
    MediaPlayer mediaPlayer;
    List<String> tabs, demos;
    TabRvAdapter tabAdapter;
    DemoRvAdapter demoAdapter;
    Intent fileIntent;
    Uri filepath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);

        tvBand = (TextView) findViewById(R.id.tvBand);
        tvSong = (TextView) findViewById(R.id.tvSong);
        tvTextAuthor = (TextView) findViewById(R.id.tvTextAuthor);
        tvMusicAuthor = (TextView) findViewById(R.id.tvMusicAuthor);
        projectAvatar = (ImageView) findViewById(R.id.ivProjectAvatar);
        tvSongText = (TextView) findViewById(R.id.tvSongText);

        rvTabs = (RecyclerView) findViewById(R.id.rvTabs);
        rvDemos = (RecyclerView) findViewById(R.id.rvDemos);

        rvTabs.setLayoutManager(new LinearLayoutManager(this));
        rvTabs.setHasFixedSize(true);
        rvDemos.setLayoutManager(new LinearLayoutManager(this));
        rvDemos.setHasFixedSize(true);

        Bundle intent = getIntent().getExtras();
        setSongInfo(getProject(intent));
    }

    public void uploadNewDemo(View v){
        fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        fileIntent.setType("*/*");
        startActivityForResult(fileIntent, 1);
    }

    public void uploadNewTab(View v){

    }

    public void sendDemoToServer(byte[] base64Demo, String fileName){
        HttpApi api = new HttpApi();
        JsonObject jsonObject;
        jsonObject = api.UploadNewDemo(Integer.toString(getProject(getIntent().getExtras()).getProjectId()), base64Demo, fileName);
        if(jsonObject.get("status").getAsString().equals("ok")) {
            //файл залит
        }
        else{
            //oh shit i'm sorry
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode){
            case 1:
                if(resultCode == RESULT_OK){
                    filepath = data.getData();
                    Cursor fileInfo = getContentResolver().query(filepath, null, null, null, null);
                    int sizeIndex = fileInfo.getColumnIndex(OpenableColumns.SIZE);
                    int nameIndex = fileInfo.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                    fileInfo.moveToNext();
                    byte[] byteFile = new byte[(int)fileInfo.getLong(sizeIndex)];
                    byte[] byteFileBase;
                    try{
                        getContentResolver().openInputStream(filepath).read(byteFile, 0, (int)fileInfo.getLong(sizeIndex));
                    }
                    catch(IOException ex){

                    }
                    byteFileBase = Base64.encode(byteFile, Base64.DEFAULT);
                    Log.i("FILENAME:", fileInfo.getString(nameIndex));
                    Log.i("FILESIZE:", Long.toString(fileInfo.getLong(sizeIndex)));
                    Log.i("FILE:", byteFileBase.toString());

                    if(fileInfo.getString(nameIndex).equals("audio/mpeg"))
                        sendDemoToServer(byteFileBase, fileInfo.getString(nameIndex));
                }
                break;
        }
    }

    public ProjectItem getProject(Bundle intent){
        return intent.getParcelable("Song");
    }

    public void setSongInfo(ProjectItem song){
        tvBand.setText(song.getProjectBrand());
        tvSong.setText("-  " + song.getProjectName());
        tvTextAuthor.setText(song.getTextAuthor());
        tvMusicAuthor.setText(song.getMusicAuthor());
        tvSongText.setText(song.getSongText());
        getAvatar(song.getProjectName(), song.getProjectMusician());
        getTabs(song);
        getDemos(song);
    }

    public void getAvatar(String projectName, String projectMusician){
        HttpApi httpApi = new HttpApi();
        Bitmap avatar;
        avatar = httpApi.getProjectAvatar(projectName, "iskanderslavenet");
        if(avatar == null){
            return;
        }
        try{
            projectAvatar.setImageBitmap(avatar);
        }
        catch(Exception ex){

        }
    }

    public void rvClick(int position, List<String> projects){
        //download tab
    }

    public void rvPlayClick(int position, List<String> demos, ProjectItem projectItem){
        Log.i("PLAYER", "PLAY!");
        String demoName;

        if(playerStatus){
            //start playing
            if(mediaPlayer == null)
                mediaPlayer = new MediaPlayer();
            else{
                mediaPlayer.stop();
                mediaPlayer.release();
            }
            try{
                mediaPlayer.setDataSource("http://localhost:3000/getsong?projectname=" + projectItem.getProjectName() +
                        "&projectmusician=" + projectItem.getProjectMusician() +
                        "&filename=" + demos.get(position));
                mediaPlayer.prepare();
                mediaPlayer.seekTo(0);
            }
            catch(IOException ex){
                Log.i("MEDIAPLAYER_ERROR", ex.getMessage());
            }
            mediaPlayer.start();
        }
        else{
            //stop playing
            mediaPlayer.pause();
        }
    }

    public void getTabs(ProjectItem song){
        HttpApi httpApi = new HttpApi();
        tabs = httpApi.GetTabs(song.getProjectName(), "iskanderslavenet");
        tabAdapter = new TabRvAdapter(tabs, ProjectActivity.this);
        tabAdapter.setOnItemClickListener(new TabRvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                rvClick(position, tabs);
            }
        });
        new ItemTouchHelper(itemTouchSimpleCallbackTab).attachToRecyclerView(rvTabs);
        rvTabs.setAdapter(tabAdapter);
        for(int i = 0; i < tabs.size(); i++){
            Log.i("HMM", tabs.get(i));
        }
    }

    public void getDemos(final ProjectItem song){
        HttpApi httpApi = new HttpApi();
        demos = httpApi.GetDemos(song.getProjectName(), "iskanderslavenet");
        demoAdapter = new DemoRvAdapter(demos, ProjectActivity.this);
        demoAdapter.setOnItemClickListener(new DemoRvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                rvPlayClick(position, demos, song);
            }
        });
        new ItemTouchHelper(itemTouchSimpleCallbackDemo).attachToRecyclerView(rvDemos);
        rvDemos.setAdapter(demoAdapter);
        for(int i = 0; i < demos.size(); i++){
            Log.i("HMM", demos.get(i));
        }
    }

    ItemTouchHelper.SimpleCallback itemTouchSimpleCallbackTab = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            tabs.remove(viewHolder.getAdapterPosition());
            tabAdapter.notifyDataSetChanged();
            //тут запрос на удаление таба
        }
    };

    public byte[] getBitmapByteArray(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    public void editProject(View view){
        Intent intent = new Intent(ProjectActivity.this, EditProjectActivity.class);
        Bundle currentIntent = getIntent().getExtras();
        ProjectItem item = currentIntent.getParcelable("Song");
        intent.putExtra("Song", item);
        //intent.putExtra("Avatar", getBitmapByteArray(((BitmapDrawable)projectAvatar.getDrawable()).getBitmap()));
        Log.i("CURINTENT", item.getMusicAuthor());
        startActivity(intent);
    }

    ItemTouchHelper.SimpleCallback itemTouchSimpleCallbackDemo = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            demos.remove(viewHolder.getAdapterPosition());
            demoAdapter.notifyDataSetChanged();
            //тут запрос на удаление демки
        }
    };
}
