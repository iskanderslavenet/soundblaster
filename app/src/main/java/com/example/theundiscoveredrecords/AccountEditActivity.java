package com.example.theundiscoveredrecords;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;

import com.example.theundiscoveredrecords.api.HttpApi;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

public class AccountEditActivity extends AppCompatActivity {

    TextInputEditText name, surname, email, phone, city, country;
    RadioButton soundDesigner, musician;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_edit);
        String profession;
        name = (TextInputEditText) findViewById(R.id.tiFirstName);
        surname = (TextInputEditText) findViewById(R.id.tiLastName);
        email = (TextInputEditText) findViewById(R.id.tiEmail);
        city = (TextInputEditText) findViewById(R.id.tiCity);
        country = (TextInputEditText) findViewById(R.id.tiCountry);
        phone = (TextInputEditText) findViewById(R.id.tiPhone);
        soundDesigner = (RadioButton) findViewById(R.id.rbSoundDesigner);
        musician = (RadioButton) findViewById(R.id.rbMusician);

        Intent intent = getIntent();

        name.setText(intent.getStringExtra("firstName"));
        surname.setText(intent.getStringExtra("lastName"));
        email.setText(intent.getStringExtra("email").replace("Почта: ", ""));
        phone.setText(intent.getStringExtra("phone").replace("Телефон: ", ""));
        city.setText(intent.getStringExtra("city").replace("Город: ", ""));
        country.setText(intent.getStringExtra("country").replace("Страна: ", ""));

        profession = intent.getStringExtra("userType");
        if(profession.equals("Звукорежиссёр"))
            soundDesigner.toggle();
        else
            musician.toggle();
    }

    public void saveUser(View view){
        Intent intent = new Intent(AccountEditActivity.this, AccountActivity.class);
        HttpApi api = new HttpApi();
        JSONObject jsonObject = new JSONObject();
        JsonElement jsonElement;
        try{
            jsonObject.put("firstName", name.getText().toString());
            jsonObject.put("lastName", surname.getText().toString());
            jsonObject.put("email", email.getText().toString());
            jsonObject.put("phone", phone.getText().toString());
            jsonObject.put("city", city.getText().toString());
            jsonObject.put("country", country.getText().toString());
            if(soundDesigner.isActivated())
                jsonObject.put("usertype", "Звукорежипссёр");
            else
                jsonObject.put("usertype", "Музыкант");
        }
        catch(JSONException ex){ }
        jsonElement = api.ChangeAccount("iskanderslavenet", "fff", jsonObject);
        if(jsonElement.getAsString().equals("ok")){
            startActivity(intent);
        }
        else{
            //тут ошибка
            Log.i("FUCKING ERROR!!!!", "ашипка");
        }
    }
}
