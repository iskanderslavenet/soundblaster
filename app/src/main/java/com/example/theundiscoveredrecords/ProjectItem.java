package com.example.theundiscoveredrecords;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class ProjectItem implements Parcelable {

    private String ProjectName;
    private String ProjectMusician;
    private String ProjectComment;
    private String ProjectBrand;
    private String ProjectPath;
    private String SongText;
    private String TextAuthor;
    private String MusicAuthor;
    private int ProjectId;

    public int getProjectId() {
        return ProjectId;
    }

    public void setProjectId(int projectId) {
        ProjectId = projectId;
    }


    public Bitmap getProjectAvatar() {
        return ProjectAvatar;
    }

    public void setProjectAvatar(Bitmap projectAvatar) {
        ProjectAvatar = projectAvatar;
    }

    public ProjectItem(String projectName, String projectMusician, String projectComment, String projectBrand, String projectPath, String songText, String textAuthor, String musicAuthor, int projectId) {
        ProjectName = projectName;
        ProjectMusician = projectMusician;
        ProjectComment = projectComment;
        ProjectBrand = projectBrand;
        ProjectPath = projectPath;
        SongText = songText;
        TextAuthor = textAuthor;
        MusicAuthor = musicAuthor;
        ProjectId = projectId;
    }

    public ProjectItem(){

    }

    private Bitmap ProjectAvatar;


    public String getProjectName() {
        return ProjectName;
    }
    public void setProjectName(String projectName) {
        ProjectName = projectName;
    }

    public String getProjectMusician() {
        return ProjectMusician;
    }
    public void setProjectMusician(String projectMusician) {
        ProjectMusician = projectMusician;
    }

    public String getProjectComment() {
        return ProjectComment;
    }
    public void setProjectComment(String projectComment) {
        ProjectComment = projectComment;
    }

    public String getProjectBrand() {
        return ProjectBrand;
    }
    public void setProjectBrand(String projectBrand) {
        ProjectBrand = projectBrand;
    }

    public String getProjectPath() {
        return ProjectPath;
    }
    public void setProjectPath(String projectPath) {
        ProjectPath = projectPath;
    }

    public String getSongText() {
        return SongText;
    }
    public void setSongText(String songText) {
        SongText = songText;
    }

    public String getTextAuthor() {
        return TextAuthor;
    }
    public void setTextAuthor(String textAuthor) {
        TextAuthor = textAuthor;
    }

    public String getMusicAuthor() {
        return MusicAuthor;
    }
    public void setMusicAuthor(String musicAuthor) {
        MusicAuthor = musicAuthor;
    }

    public byte[] getBitmapByteArray(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    public Bitmap getBitmapFromByteArray(byte[] avatar){
        ByteArrayInputStream stream = new ByteArrayInputStream(avatar, 0, avatar.length);
        Bitmap bitmap = BitmapFactory.decodeStream(stream);
        return bitmap;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getProjectName());
        parcel.writeString(getProjectBrand());
        parcel.writeString(getProjectComment());
        parcel.writeString(getProjectMusician());
        parcel.writeString(getProjectPath());
        parcel.writeString(getSongText());
        parcel.writeString(getTextAuthor());
        parcel.writeString(getMusicAuthor());
        parcel.writeInt(getProjectId());
        //parcel.writeByteArray(getBitmapByteArray(getProjectAvatar()));
    }

    public static final Parcelable.Creator<ProjectItem> CREATOR = new Parcelable.Creator<ProjectItem>(){

        //private byte[] projectAvatarByteArray;

        @Override
        public ProjectItem createFromParcel(Parcel parcel) {
            String name = parcel.readString();
            String brand = parcel.readString();
            String comment = parcel.readString();
            String musician = parcel.readString();
            String projectPath = parcel.readString();
            String songText = parcel.readString();
            String textAuthor = parcel.readString();
            String musicAuthor = parcel.readString();
            int projectId = parcel.readInt();
            //parcel.readByteArray(projectAvatarByteArray);
            //Bitmap projectAvatar = new ProjectItem().getBitmapFromByteArray(projectAvatarByteArray);
            return new ProjectItem(name, brand, comment, musician, projectPath, songText, textAuthor, musicAuthor, projectId);
        }

        @Override
        public ProjectItem[] newArray(int i) {
            return new ProjectItem[i];
        }
    };
}
